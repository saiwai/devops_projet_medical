from model.db import Connect
class DeplacementModel:

    def __init__(self):
        self.conn = Connect.log()

    def fetch_all(self):

        self.conn.execute("""
            SELECT deplacement.id,deplacement.infirmiere_id, deplacement.patient_id, DATE_FORMAT(deplacement.`date`, "%d/%m/%Y") as date, infirmiere.numeroProfessionnel,
                infirmiere.nom, infirmiere.prenom, numerosecuritesocial, patient.nom,
                patient.prenom, numero, rue, cp,ville, cout  
            FROM deplacement
            INNER JOIN infirmiere ON deplacement.infirmiere_id = infirmiere.id
            INNER join patient ON patient.id = deplacement.patient_id
            INNER JOIN adresse ON patient.adresse_id = adresse.id
            ORDER BY deplacement.date DESC; """)

        rows = self.conn.fetchall()
        return rows
    
    def fetch_deplacement_by_id(self,id):
        self.conn.execute(f"""SELECT DEPLACEMENT.ID, DEPLACEMENT.INFIRMIERE_ID, deplacement.patient_id, DATE_FORMAT(DEPLACEMENT.`date`, "%d/%m/%Y"), INFIRMIERE.NUMEROPROFESSIONNEL,
                            INFIRMIERE.NOM, INFIRMIERE.PRENOM, PATIENT.NUMEROSECURITESOCIAL, PATIENT.NOM,
                            PATIENT.PRENOM, ADRESSE.NUMERO, ADRESSE.RUE, ADRESSE.CP,ADRESSE.VILLE, DEPLACEMENT.COUT  FROM DEPLACEMENT
                            inner JOIN INFIRMIERE on (deplacement.infirmiere_id = infirmiere.id)
                            inner join PATIENT on (patient.id = deplacement.patient_id)
                            inner JOIN ADRESSE on (patient.adresse_id = adresse.id)
                            WHERE deplacement.id = '{id}'""")
        row = self.conn.fetchall()
        return row
    
    def fetch_deplacement_by_infirmiere(self,id):
        self.conn.execute(f"""
            SELECT deplacement.id, deplacement.infirmiere_id, DATE_FORMAT(deplacement.`date`, "%d/%m/%Y") as date, infirmiere.numeroProfessionnel,
                infirmiere.nom, infirmiere.prenom, numerosecuritesocial, patient.nom,
                patient.prenom, numero, rue, cp ,ville, cout  
            FROM deplacement
                INNER JOIN infirmiere ON deplacement.infirmiere_id = infirmiere.id
                INNER JOIN  patient ON patient.id = deplacement.patient_id
                INNER JOIN adresse ON patient.adresse_id = adresse.id
            WHERE deplacement.infirmiere_id = '{id}'
            ORDER by deplacement.date DESC;
        """)
        rows = self.conn.fetchall()
        return rows

    def add_deplacement(self,deplacement):
        self.conn.execute(f""" INSERT INTO DEPLACEMENT (patient_id, date, cout, infirmiere_id) 
                                    VALUES('{int(deplacement.get('id_patient'))}',
                                    '{deplacement.get('date')}','{deplacement.get('cout')}','{int(deplacement.get('id_infirmiere'))}')""")
     

    def update_deplacement(self,data):
        self.conn.execute(f""" UPDATE deplacement SET patient_id = '{int(data.get('patient_id'))}', 
                                cout ='{(data.get('cout'))}', infirmiere_id = '{int(data.get('infirmiere_id'))}'
                                WHERE id = '{int(data.get('id'))}' """)

    def delete_deplacement_by_id(self,id):
        self.conn.execute(f""" DELETE FROM deplacement WHERE id = {id}; """)
