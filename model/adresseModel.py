from model.db import Connect


class AdresseModel():
    def __init__(self):
        self.conn = Connect.log()
    
    def is_insert_adresse(self, formData):
        self.conn.execute(
            f""" 
            SELECT id
            FROM adresse 
            WHERE numero = '{formData.get('numero')}' AND rue = '{formData.get('rue')}' 
                AND cp = '{formData.get('cp')}' AND ville = '{formData.get('ville')}'
            """
        )
        rows = self.conn.fetchone()
        return rows
        
        
    def insert_adresse(self, formData):
        requete = """ INSERT INTO `adresse` (`numero`, `rue`, `cp`, `ville`) VALUES (%s, %s, %s, %s) """
        value = (formData.get('numero'), formData.get('rue'),formData.get('cp'), formData.get('ville'))
        self.conn.execute(requete,value)