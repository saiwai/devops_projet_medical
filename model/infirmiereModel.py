from model.db import Connect


class InfirmiereModel():
    def __init__(self):
        self.conn = Connect.log()
    
    def fetch_all(self):
        ''' requete permettant de recuperer tous les informations de infirmiére
            avec son adresse
        '''

        self.conn.execute(
            """ 
            SELECT infirmiere.id, nom, prenom, numeroProfessionnel, telPro, telPerso, estActive, numero, rue, cp, ville
            FROM infirmiere
                INNER JOIN adresse ON infirmiere.adresse_id = adresse.id 
            ORDER BY estActive DESC
            """
        )
        rows = self.conn.fetchall()

        return rows

    def fetch_infirmiere(self, formData):
        ''' requete permettant de recuperer les informations d'une infirmiere grace à son id
        '''

        self.conn.execute(
            f""" 
            SELECT infirmiere.id, nom, prenom, numeroProfessionnel, telPro, telPerso, estActive, numero, rue, cp, ville
            FROM infirmiere
                INNER JOIN adresse ON infirmiere.adresse_id = adresse.id 
            WHERE infirmiere.id = {formData.get('id')}
            """
        )
        rows = self.conn.fetchall()
        return rows

    
    def insert_infirmiere(self, formData, id_adresse):
        self.conn.execute(
            f"""
            INSERT INTO `infirmiere` (`adresse_id`, `numeroProfessionnel`, `nom`, `prenom`, `telPro`, `telPerso`, `estActive`)
                VALUES('{id_adresse}', '{formData.get('numeroProfessionnel')}', '{formData.get('nom')}', '{formData.get('prenom')}','{formData.get('telPro')}', '{formData.get('telPerso')}', 1)
            """
        )
        #return formData.get('numeroProfessionnel')

    def update_infirmiere(self, data):
        self.conn.execute(
            f""" UPDATE infirmiere SET nom = '{data.get('nom')}', prenom = '{data.get('prenom')}',
                telPerso = '{data.get('telPerso')}' WHERE id = '{data.get('id')}'"""
        )

    def delete_infirmiere(self, id, actif):
        self.conn.execute(
            f""" UPDATE infirmiere SET estActive = '{actif}' WHERE id = '{id}'"""
        )

    def search_adresse_infirmiere(self, id):
        self.conn.execute(
            f"""SELECT adresse_id FROM infirmiere WHERE adresse_id = {id}"""
        )

    def delete_adresse(self, id):
        self.conn.execute(
            f"""UPDATE infirmiere SET adresse_id = null WHERE adresse_id = {id}"""
        )