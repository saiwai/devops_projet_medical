from model.db import Connect


class AdresseModel:
    def __init__(self):
        self.conn = Connect.log()

    def fetch_all(self):
        self.conn.execute(
            """
            SELECT adresse.id, adresse.numero, adresse.rue, adresse.cp, adresse.ville
            FROM adresse
            INNER JOIN patient ON patient.adresse_id = adresse.id
            INNER JOIN infirmiere ON infirmiere.adresse_id = adresse.id;
            """
        )
        rows = self.conn.fetchall()
        return rows

    def update(self, data):
        self.conn.execute(
            f"""
            UPDATE adresse SET numero = '{data.get('numero')}', rue = '{data.get('rue')}', cp = '{data.get('cp')}', ville = '{data.get('ville')}'
            WHERE id = '{int(data.get('id'))}'
            """
        )

    def deleteById(self, id):
        self.conn.execute(
            f""" DELETE FROM adresse WHERE id = {id} """
        )

