from os import rename
from flask import render_template
from flask.templating import render_template_string

class DeplacementController():

    def fetch_deplacement(self, model):
        return render_template('liste_deplacement.html', obj = model.fetch_all())
    
    def fetch_deplacement_by_id(self, model, id):
        return render_template('fiche_deplacement.html', obj = model.fetch_deplacement_by_id(id))

    def fetch_deplacement_by_infirmiere(self, model, id):
        return render_template('liste_deplacement_infirmiere.html', obj = model.fetch_deplacement_by_infirmiere(id))

    def vue_add_deplacement(self):
        return render_template('form_add_deplacement.html')

    def add_deplacement(self,model,deplacement):
        return model.add_deplacement(deplacement)

    def vue_update_deplacement(self,data):
       return render_template('form_update_deplacement.html', data = data)

    def update_deplacement(self,model,data):
       return model.update_deplacement(data)
    
    def delete_deplacement_by_id(self, model, id):
        model.delete_deplacement_by_id(id)