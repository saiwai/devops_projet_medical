from flask import render_template



class InfirmiereController:

    def liste_infirmiere(self, infirmiereModel):
        return render_template('liste-infirmiere.html', obj=infirmiereModel.fetch_all())

    def vue_add_infirmiere(self):
        return render_template('formulaire-infirmiere.html')

    def insert_infirmiere(self, infirmiereModel, adresse, formData):
        return infirmiereModel.insert_infirmiere(formData, adresse)

    def vue_update_infirmiere(self, formData, infirmiereModel):
        return render_template('formulaire_update_infirmiere.html',
                               information_infirmiere=infirmiereModel.fetch_infirmiere(formData))

    def update_infirmiere(self, infirmiere_model, data):
        return infirmiere_model.update_infirmiere(data)

    def delete_infirmiere(self, infirmiere_model, id, active):
        if active == 1:
            return infirmiere_model.delete_infirmiere(id, 0)
        else:
            return infirmiere_model.delete_infirmiere(id, 1)

