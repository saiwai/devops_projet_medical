from flask import render_template


class AdresseController():

    def fetch_adresse(self, model):
        return render_template('adresses.html', obj=model.fetch_all())

    def vue_update(self, data):
        return render_template('adresse_update_form.html', data=data)

    def update(self, model, data):
        model.update(data)

    def delete_by_id(self, model, id, infirmiere, patient):
        # infirmiere = infirmiere.search_adresse_infirmiere(id)
        # if infirmiere:
        infirmiere.delete_adresse(id)
        patient.delete_adresse_patient(id)
        return model.deleteById(id)
