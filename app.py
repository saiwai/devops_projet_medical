from flask import Flask, request
from controller.infirmiereController import InfirmiereController
from controller.adresseController import AdresseController
from controller.deplacement_controller import DeplacementController
from controller.index import Index
from model.patient import Patient

from model.infirmiereModel import InfirmiereModel
from model.adresseModel import AdresseModel
from model.deplacement_model import DeplacementModel

from controller.adresse import AdresseController
from model.adresse import AdresseModel

from werkzeug.utils import redirect


app = Flask(__name__)


adresse_controller = AdresseController()
infirmiereModel = InfirmiereModel()
adresseModel = AdresseModel()
deplacement_model = DeplacementModel()
deplacement_controller = DeplacementController()
adresModel = AdresseModel()
adresContr = AdresseController()
infirmiere_controller = InfirmiereController()
infirmiereModel = InfirmiereModel()
patient = Patient()
index_vue = Index()
URI = "http://localhost:5000/"


@app.route('/')
def index():
    return index_vue.vue_index()


@app.route('/liste-infirmiere/')
def liste_infirmiere():
    return infirmiere_controller.liste_infirmiere(infirmiereModel)


@app.route('/ajout-infirmiere/')
def formulaire_add_infirmiere():
    return infirmiere_controller.vue_add_infirmiere()


@app.route('/addInfirmiere', methods=['POST', 'GET'])
def insert_infirmiere():
    formData = request.form
    adresse = adresse_controller.insert_adresse(adresseModel, formData)
    infirmiere_controller.insert_infirmiere(infirmiereModel, adresse[0], formData)
    return redirect('http://localhost:5000/liste-infirmiere')


@app.route('/update-infirmiere/')
def update_infirmiere():
    formData = request.args
    return infirmiere_controller.vue_update_infirmiere(formData, infirmiereModel)


@app.route('/infirmiere-update', methods=['POST', 'GET'])
def infirmiere_update():
    formData = request.form
    infirmiere_controller.update_infirmiere(infirmiereModel, formData)
    return redirect('http://localhost:5000/liste-infirmiere')


@app.route('/delete-infirmiere/<int:id>/<int:active>')
def delete_infirmiere(id, active):
    infirmiere_controller.delete_infirmiere(infirmiereModel, id, active)
    return redirect('http://localhost:5000/liste-infirmiere')


@app.route('/liste_deplacement/')
def liste_deplacement():
    return deplacement_controller.fetch_deplacement(deplacement_model)


@app.route('/liste_deplacement_infirmiere/<int:id>')
def liste_deplacement_infirmiere(id):
    return deplacement_controller.fetch_deplacement_by_infirmiere(deplacement_model,id)


@app.route('/form_add_deplacement/')
def form_add_deplacement():
    return deplacement_controller.vue_add_deplacement()


@app.route('/add_deplacement', methods = ['POST','GET'])
def add_deplacement():
    formData = request.form
    deplacement_controller.add_deplacement(deplacement_model,formData)
    return redirect("http://localhost:5000/liste_deplacement")


@app.route('/form_update_deplacement/')
def formulaire_update():
    formData = request.args
    return deplacement_controller.vue_update_deplacement(formData)


@app.route('/update_deplacement', methods=['POST','GET'])
def update_deplacement():
    formData = request.form
    deplacement_controller.update_deplacement(deplacement_model,formData)
    return redirect("http://localhost:5000/liste_deplacement")


@app.route('/adresse_update_form/')
def formulaire_update_adresse():
    formData = request.args
    return adresContr.vue_update(formData)


@app.route('/adresse_update/', methods=['POST', 'GET'])
def update_adresse():
    formData = request.form
    adresContr.update(adresModel, formData)
    return redirect(URI)


@app.route('/adresses/')
def adresses():
    return adresContr.fetch_adresse(adresModel)


@app.route('/delete/<int:id>')
def delete(id):
    adresContr.delete_by_id(adresModel, id, infirmiereModel, patient)
    return redirect("http://localhost:5000/adresses")


@app.route('/delete_deplacement/<int:id>')
def delete_deplacement_by_id(id):
    deplacement_controller.delete_deplacement_by_id(deplacement_model,id)
    return redirect("http://localhost:5000/liste_deplacement")

@app.route('/fiche_deplacement/<int:id>')
def fiche_deplacement_by_id(id):
    return deplacement_controller.fetch_deplacement_by_id(deplacement_model,id)