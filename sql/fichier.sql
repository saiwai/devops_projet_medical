ALTER TABLE `infirmiere`
CHANGE `telPro` `telPro` varchar(20) COLLATE 'utf8_general_ci' NOT NULL AFTER `prenom`,
CHANGE `telPerso` `telPerso` varchar(20) COLLATE 'utf8_general_ci' NULL AFTER `telPro`;
ADD COLUMN `estActive` TINYINT(1) NULL DEFAULT 1 AFTER `telPerso`;